import caldav
from datetime import datetime, timedelta, timezone
from zoneinfo import ZoneInfo
from apscheduler.schedulers.blocking import BlockingScheduler
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, ConversationHandler, CommandHandler, MessageHandler, Filters, CallbackContext
import dateparser
import pytimeparse
import icalendar
import config

calendar = caldav.DAVClient(
    url=config.CALDAV_URL,
    username=config.CALDAV_USER,
    password=config.CALDAV_PASSWORD
).calendar(url=config.CALENDAR)
updater = Updater(config.TELEGRAM_TOKEN)
scheduler = BlockingScheduler()

def render_event(vevent: caldav.vobject.icalendar.VEvent) -> str:
    text = (f'*{vevent.summary.value}*\n'
            f'📅 {vevent.dtstart.value.astimezone(ZoneInfo(config.TIMEZONE)).strftime("%d/%m %H:%M")}\n'
            f'🕒 {str(vevent.dtend.value - vevent.dtstart.value)[:-3]}h')
    try:
        text += f'\n📍 {vevent.location.value}'
    except AttributeError:
        pass
    try:
        text += f'\n👤 {vevent.description.value}'
    except AttributeError:
        pass
    return text

def send_reminder(vevent: caldav.vobject.icalendar.VEvent) -> None:
    for group in config.REMINDER_GROUPS:
        updater.bot.send_message(
            group,
            f'_Recordatorio:_\n{render_event(vevent)}',
            parse_mode='Markdown')

def schedule_existing() -> None:
    for vcal in calendar.date_search(
                    start=datetime.now(),
                    end=datetime.now()+timedelta(weeks=config.WEEKS_ADVANCE),
                    expand=True
                ):
        for vevent in vcal.instance.getChildren():
            if vevent.name == 'VEVENT':
                schedule_event(vevent)

def schedule_event(vevent: caldav.vobject.icalendar.VEvent) -> None:
    start = datetime.fromisoformat(str(vevent.dtstart.value)).astimezone(ZoneInfo(config.TIMEZONE))
    scheduler.add_job(send_reminder, 'date', run_date=start - timedelta(days=1), args=[vevent])
    scheduler.add_job(send_reminder, 'date', run_date=start - timedelta(hours=1), args=[vevent])

schedule_existing()
# exit()

def help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text((
        'Hola!\n'
        'Manda /nuevo para crear un nuevo evento, y te iré preguntando los datos para registrarlo.\n'
        'La fecha de inicio y duración la puedes escribir en lenguaje natural, '
        'por ejemplo `mañana 19:00` y `2h 30min`, pero a veces puede pasar que no lo entienda, sorry :(\n'
        'En cualquier momento puedes mandar /cancelar para parar el registro'
    ), reply_markup=ReplyKeyboardRemove(), parse_mode='Markdown')

NAME, START_DT, DURATION, LOCATION, CONTACT, CONFIRM = range(6)

def new_event(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(
        'Hola de nuevo! Cómo se debe llamar el evento?\n',
        reply_markup=ReplyKeyboardRemove())
    return NAME

def name(update: Update, context: CallbackContext) -> int:
    context.user_data['name'] = update.message.text
    update.message.reply_text(
        'Genial! Ahora dime cuándo empieza',
        reply_markup=ReplyKeyboardRemove())
    return START_DT

def start_dt(update: Update, context: CallbackContext) -> int:
    context.user_data['start'] = update.message.text
    update.message.reply_text((
        'Cuanto dura?\n'
        f'Para el defecto de {config.DEFAULT_DURATION} puedes mandar /saltar'
    ), reply_markup=ReplyKeyboardRemove())
    return DURATION

def duration(update: Update, context: CallbackContext) -> int:
    if '/saltar' in update.message.text:
        context.user_data['duration'] = config.DEFAULT_DURATION
    else:
        context.user_data['duration'] = update.message.text

    update.message.reply_text((
        'Dónde es?\n'
        'Para dejarlo vacío puedes mandar /saltar'
    ), reply_markup=ReplyKeyboardRemove())
    return LOCATION

def location(update: Update, context: CallbackContext) -> int:
    if '/saltar' in update.message.text:
        if 'location' in context.user_data:
            del context.user_data['location']
    else:
        context.user_data['location'] = update.message.text

    update.message.reply_text((
        'Te quieres poner como persona de contacto? (tu @ de Telegram)\n'
        'Si quieres poner a otra persona, escribe su @'
    ), reply_markup=ReplyKeyboardMarkup(
            [['Sí', 'No']],
            one_time_keyboard=True
    ))
    return CONTACT

def contact(update: Update, context: CallbackContext) -> int:
    if update.message.text == 'Sí':
        if update.message.from_user.username:
            context.user_data['contact'] = '@'+update.message.from_user.username
        else:
            context.user_data['contact'] = update.message.from_user.name
    elif update.message.text == 'No':
        if 'contact' in context.user_data:
            del context.user_data['contact']
    else:
        context.user_data['contact'] = update.message.text

    event = create_event(context.user_data)
    if not event:
        update.message.reply_text((
            'Ha habido un error, porfa intentalo de nuevo\n'
            'Probablemente no entiendo la fecha o la duración'
        ), reply_markup=ReplyKeyboardRemove())
        update.message.reply_text('Cuándo empieza?')
        return START_DT

    context.user_data['event'] = event
    update.message.reply_text(
        f'{render_event(event.instance.vevent)}\n\nEs correcto?',
        reply_markup=ReplyKeyboardMarkup(
            [['Sí', 'No, volver a empezar']],
            one_time_keyboard=True
        ),
        parse_mode='Markdown'
    )
    return CONFIRM

def confirm(update: Update, context: CallbackContext) -> int:
    if update.message.text == 'Sí':
        save_event(context.user_data['event'])
        update.message.reply_text(
            'Registrado!',
            reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    else:
        update.message.reply_text(
            'Ok! Cómo se debe llamar el evento?',
            reply_markup=ReplyKeyboardRemove())
        return NAME

def cancel(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(
        'Ok! Escríbeme de nuevo cuando quieras',
        reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END

def create_event(user_data: dict) -> caldav.Event:
    try:
        start = dateparser.parse(
            user_data['start'],
            locales=['es-419'],
            settings={
                'DATE_ORDER': 'DMY',
                'PREFER_DATES_FROM': 'future'
            }
        ).astimezone(timezone.utc)
    except TypeError:
        print('Error with start time:', user_data['start'])
        return None

    try:
        end = start + timedelta(seconds=pytimeparse.parse(user_data['duration']))
    except TypeError:
        print('Error with duration', user_data['duration'])
        return None

    ical = icalendar.Calendar()
    ievent = icalendar.Event()

    ievent.add('summary', user_data['name'])
    ievent.add('dtstart', start)
    ievent.add('dtend'  , end)
    ievent.add('dtstamp', datetime.now())
    if 'location' in user_data:
        ievent.add('location', user_data['location'])
    if 'contact' in user_data:
        ievent.add('description', 'Contacto: '+user_data['contact'])

    ical.add_component(ievent)

    return caldav.Event(data=ical.to_ical()) 

def save_event(event: caldav.Event) -> None:
    calendar.save_event(event.data)
    schedule_event(event.instance.vevent)

updater.dispatcher.add_handler(CommandHandler(['start', 'ayuda'], help))

TEXT_NO_COMMAND = Filters.text & ~Filters.command
updater.dispatcher.add_handler(ConversationHandler(
    entry_points=[CommandHandler('nuevo', new_event)],
    states={
        NAME: [MessageHandler(TEXT_NO_COMMAND, name)],
        START_DT: [MessageHandler(TEXT_NO_COMMAND, start_dt)],
        DURATION: [MessageHandler(TEXT_NO_COMMAND, duration), CommandHandler('saltar', duration)],
        LOCATION: [MessageHandler(TEXT_NO_COMMAND, location), CommandHandler('saltar', location)],
        CONTACT: [MessageHandler(TEXT_NO_COMMAND, contact), CommandHandler('saltar', contact)],
        CONFIRM: [MessageHandler(TEXT_NO_COMMAND, confirm)],
    },
    fallbacks=[CommandHandler('cancelar', cancel)]
))

updater.start_polling()
scheduler.start()