## Calendario Bot

Bot de Telegram para registrar eventos a un calendario y recibir recordatorios

## Install and run

1. Clone this repo
2. Edit `config.py` with your information
3. pip3 install -r requirements.txt
4. python3 calendariobot.py